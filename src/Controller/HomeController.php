<?php

namespace App\Controller;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\RestApiConsumers\InternetChuckNorrisDatabaseClient;
use App\Utils\Fibonacci;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    // TODO: move this to a config
    private $listingChunkSize = 20;

    public function __construct(InternetChuckNorrisDatabaseClient $icndb_api, TwitterOAuth $twitter_api)
    {
        $this->icndbApi = $icndb_api;
        $this->twitterApi = $twitter_api;
    }

    public function index($handle1, $handle2, $method) : Response
    {
        $handle1_tweets = $this->twitterApi->get(
            'statuses/user_timeline',
            ['count' => $this->listingChunkSize, 'screen_name' => $handle1]
        );
        $handle2_tweets = $this->twitterApi->get(
            'statuses/user_timeline',
            ['count' => $this->listingChunkSize, 'screen_name' => $handle2]
        );
        $combined_data = array_merge(
            $this->normalizeTwitterData($handle1_tweets),
            $this->normalizeTwitterData($handle2_tweets)
        );
        usort($combined_data, function ($a, $b) {
            return $b['date']->format('U') <=> $a['date']->format('U');
        });

        $count = count($combined_data);
        // only bother with getting replacements if there's at leas 3 elements
        if ($count > 2) {
            // instead of iterating through elements and replacing
            // we're going to build an array of indices that should be replaced
            $replacement_indices = [];
            if ($method == 'fib') {
                $replacement_indices = Fibonacci::inRange(3, $count);
            } else {
                $replacement_indices = range(3, $count, 3);
            }
            // build an array
            $chuck_norris_replacement_data = array_combine(
                // with indices from $replacement_indices
                $replacement_indices,
                // and data from icndb
                $this->normalizeChuckNorrisData(
                    // minimizing traffic by fetching
                    // exact amount of data needed in batch
                    $this->icndbApi->getJokes(count($replacement_indices))
                )
            );
            // prepend one element, so indexing of real data starts at 1, yuck
            array_unshift($combined_data, null);
            // replace twitter data with superior Chuck Norris Jokes
            $combined_data = array_replace(
                $combined_data,
                $chuck_norris_replacement_data
            );
            // remove first element preserving index
            unset($combined_data[0]);
        }

        return $this->render('list.html.twig', ['list' => $combined_data]);
    }

    private function normalizeTwitterData($data) : array
    {
        $result = [];

        if (!is_array($data)) {
            return $result;
        }

        foreach ($data as $tweet) {
            $result[] = [
                'date' => new \DateTime($tweet->created_at),
                'source' => 'twitter/' . $tweet->user->screen_name,
                'text' => $tweet->text,
            ];
        }

        return $result;
    }

    private function normalizeChuckNorrisData($data) : array
    {
        return array_map(
            function ($item) {
                return [
                    'date' => null,
                    'source' => 'icndb',
                    'text' => html_entity_decode($item->joke)
                ];
            },
            $data
        );
    }
}
