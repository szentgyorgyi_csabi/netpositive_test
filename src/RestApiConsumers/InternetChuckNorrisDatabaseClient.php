<?php

namespace App\RestApiConsumers;

class InternetChuckNorrisDatabaseClient
{
    /**
     * @var string $baseUrl Internet Chuck Norris Database API url
     */
    protected static $baseUrl = 'http://api.icndb.com';

    /**
     * @var string|null $firstName possible first name replacement
     */
    protected $firstName;

    /**
     * @var string|null $lastName possible last name replacement
     */
    protected $lastName;

    /**
     * @var array $include whitelist of categories for results
     */
    protected $include = [];

    /**
     * @var array $exclude blacklist of categories for results
     */
    protected $exclude = [];

    /**
     * @var array $path of called url
     */
    protected $path = [];

    /**
     * @throws Exception when cUrl library is missing
     */
    public function __construct()
    {
        if (!function_exists('curl_init')) {
            throw new Exception('Chuch Norris disapproves of your lack of cUrl');
        }
    }

    /**
     * Sets a first name replacement for 'Chuck'
     * not that I recommend it
     *
     * @param string $first_name
     * @return self
     */
    public function setFirstName(string $first_name) : self
    {
        $this->firstName = $first_name;

        return $this;
    }

    /**
     * Sets a last name replacement for 'Norris'
     * thread carefully
     *
     * @param string $last_name
     * @return self
     */
    public function setLastName(string $last_name) : self
    {
        $this->lastName = $last_name;

        return $this;
    }

    /**
     * Restrict joke results to provided categories
     *
     * @param string|array $categories category or array of categories
     * @return self
     */
    public function includeCategories($categories) : self
    {
        $this->include = array_filter(array_merge($this->include, (array)$categories));

        return $this;
    }

    /**
     * Exclude results from provided categories
     *
     * @param string|array $categories category or array of categories
     * @return type
     */
    public function excludeCategories($categories) : self
    {
        $this->exclude = array_filter(array_merge($this->exclude, (array)$categories));

        return $this;
    }

    /**
     * Retrieve an array of jokes
     *
     * @param int $count number of jokes to fetch
     * @return array
     */
    public function getJokes(int $count = 0)
    {
        $this->path = ['jokes', 'random'];
        if ($count > 1) {
            $this->path[] = $count;
        }

        $jokes = $this->fetch();
        if ($count < 2) {
            $jokes = [$jokes];
        }

        return $jokes;
    }

    /**
     * Retrieve a single joke randomly
     * or specific one if $id is provided
     *
     * @param int $id optional id
     * @return object
     */
    public function getSingleJoke(int $id = 0)
    {
        if (!$id) {
            return $this->getJokes()[0];
        }

        $this->path = ['jokes', $id];

        return $this->fetch();
    }

    /**
     * Retrieve joke categories
     *
     * @return array
     */
    public function getcategories()
    {
        $this->path = ['categories'];

        return $this->fetch();
    }

    /**
     * Retrieve joke count
     *
     * @return int
     */
    public function getJokeCount()
    {
        $this->path = ['jokes', 'count'];

        return $this->fetch();
    }

    /**
     * Sends requests to the Chuck Norris Database API
     *
     * @throws Exception on failed/bad request
     * @return mixed
     */
    protected function fetch()
    {
        $curl_handle = curl_init();
        curl_setopt_array(
            $curl_handle,
            [
                CURLOPT_URL => $this->buildUrl(),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 7,
                CURLOPT_FAILONERROR => true,
            ]
        );
        $response = curl_exec($curl_handle);
        $error = curl_error($curl_handle);
        curl_close($curl_handle);

        $response = json_decode($response);

        $this->resetProperties();

        if (!$error) {
            if (!isset($response->value)) {
                $error = 'Chuck Norris roundhouse kicked your request. This is not a joke. Ok, maybe it is, but not the kind you expected.';
            } elseif (isset($response->type) && $response->type != 'success') {
                $error = $response->value;
            }
        }

        if ($error) {
            throw new \Exception($error);
        }

        return $response->value;
    }

    /**
     * Helper method that builds the request url
     *
     * @return string
     */
    protected function buildUrl() : string
    {
        $url = static::$baseUrl . '/' . implode('/', $this->path);

        $query_string = $this->buildQueryString();
        if ($query_string) {
            $url .= "?{$query_string}";
        }

        return $url;
    }

    /**
     * helper method that builds the query string part of the url
     *
     * @return string
     */
    protected function buildQueryString() : string
    {
        $parameters = [];
        if ($this->firstName) {
            $parameters['firstName'] = $this->firstName;
        }
        if ($this->lastName) {
            $parameters['lastName'] = $this->lastName;
        }
        if ($this->include) {
            $parameters['limitTo'] = implode(',', $this->include);
        }
        if ($this->exclude) {
            $parameters['exclude'] = implode(',', $this->exclude);
        }

        return http_build_query($parameters);
    }

    /**
     * Resets object properties to allow
     * reusing the oject for multiple requests
     *
     * @return void
     */
    protected function resetProperties()
    {
        $this->firstName = null;
        $this->lastName = null;
        $this->include = [];
        $this->exclude = [];
    }
}
