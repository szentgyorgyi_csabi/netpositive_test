<?php

namespace App\Utils;

class Number
{
    /**
     * check if argument is a perfect square
     *
     * @param int $number
     *
     * @return bool
     */
    public static function isPerfectSquare(int $number) : bool
    {
        return ((int)sqrt($number)) ** 2 === $number;
    }

    /**
     * check if argument is a positive integer
     *
     * @param mixed $number
     *
     * @return bool
     */
    public static function isPositiveInteger($number) : bool
    {
        return $number > 0 && is_int($number);
    }
}
