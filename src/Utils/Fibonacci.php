<?php

namespace App\Utils;

use App\Utils\Number;

class Fibonacci
{
    /**
     * Checks if $number is a Fibonacci number
     *
     * @param mixed $number to be checked
     *
     * @return bool
     */
    public static function isFibonacci($number) : bool
    {
        return Number::isPositiveInteger($number)
            and Number::isPerfectSquare(5 * $number * $number + 4)
            || Number::isPerfectSquare(5 * $number * $number - 4);
    }

    /**
     * Provides an array of Fibonacci numbers
     * in an inclusive range between $lower and $upper bounds
     *
     * @param int $lower bound of the range
     * @param int $upper bound of the range
     *
     * @return array of Fibonacci numbers
     */
    public static function inRange(int $lower, int $upper) : array
    {
        if (!(Number::isPositiveInteger($lower) && Number::isPositiveInteger($upper))) {
            throw new \InvalidArgumentException("Upper and lower bounds must be postive integers, {$lower} and {$upper} given");
        }

        $result = [];

        // find first 2 elements between $lower and $upper bounds
        for ($i = $lower; $i <= $upper; $i++) {
            if (static::isFibonacci($i)) {
                $result[] = $i;
            }
            if (count($result) == 2) {
                break;
            }
        }

        // fill in the rest
        if (count($result) == 2 && end($result) < $upper) {
            while (true) {
                $potential_element = end($result) + prev($result);
                if ($potential_element <= $upper) {
                    $result[] = $potential_element;
                } else {
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Provides an array of the first $n Fibonacci numbers
     *
     * @param int $n number of Fibonacci numbers requested
     *
     * @return array of Fibonacci numbers
     */
    public static function first(int $n) : array
    {
        if (!Number::isPositiveInteger($n)) {
            throw new \InvalidArgumentException("Argument must be postive integer, {$n} given");
        }

        $result = [];

        for ($i = 0; $i < $n; $i++) {
            if ($i  <= 1) {
                $result[] = 1;
            } else {
                $result[] = end($result) + prev($result);
            }
        }

        return $result;
    }
}
