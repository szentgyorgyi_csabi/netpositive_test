<?php

namespace App\Tests\Utils;

use App\Utils\Fibonacci;
use PHPUnit\Framework\TestCase;

class FibonacciTest extends TestCase
{

    /**
     * @dataProvider fibonacciProvider
     */
    public function testIsFibonacci($number, $expected)
    {
        $result = Fibonacci::isFibonacci($number);
        $this->assertEquals($expected, $result);
    }

    public function fibonacciProvider()
    {
        return [
            'fibonacci_integer_1' => [1, true],
            'fibonacci_integer_5' => [5, true],
            'zero' => [0, false],
            'negative_number' => [-2, false],
            'float' => [3.1, false],
            'float_coercible_to_fibonacci_integer' => [5.0, false],
            'string_coercible_to_fibonacci_integer' => ['13', false],
        ];
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInRangeLowerBoundInvalidArgumentException()
    {
        Fibonacci::inRange(-1, 2);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInRangeUpperrBoundInvalidArgumentException()
    {
        Fibonacci::inRange(3, 0);
    }

    /**
     * @dataProvider inRangeProvider
     */
    public function testInRange($start, $end, $expected)
    {
        $result = Fibonacci::inRange($start, $end);
        $this->assertEquals($expected, $result);
    }

    public function inRangeProvider()
    {
        return [
            'range_1_10' => [1, 10, [1, 2, 3, 5, 8]],
            'range_4_60' => [4, 60, [5, 8, 13, 21, 34, 55]],
            'range_bounds_match' => [144, 144, [144]],
            'no_fibonacci_numbers_between' => [22, 30, []],
            'lower_bound_larger_than_upper_bound' => [40, 2, []],
        ];
    }

    /**
     * @dataProvider firstProvider
     */
    public function testFirst($number, $expected)
    {
        $result = Fibonacci::first($number);
        $this->assertEquals($expected, $result);
    }

    public function firstProvider()
    {
        return [
            'first_1' => [1, [1]],
            'first_2' => [2, [1, 1]],
            'first_11' => [11, [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]],
        ];
    }
}
